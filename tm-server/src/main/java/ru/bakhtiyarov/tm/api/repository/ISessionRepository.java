package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    List<Session> findAll(String userId);

    boolean removeAll(String userId);

    @NotNull
    List<Session> removeAll();

    @NotNull
    List<Session> findAll();

}
