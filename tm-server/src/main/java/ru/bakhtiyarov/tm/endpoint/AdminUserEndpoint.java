package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IAdminUserEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.api.service.converter.IConverterLocator;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    public AdminUserEndpoint() {
        super(null);
    }

    @NotNull
    public AdminUserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable User user = serviceLocator.getUserService().lockUserByLogin(login);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Override
    public @Nullable UserDTO unLockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable User user = serviceLocator.getUserService().unLockUserByLogin(login);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    public UserDTO removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable User user = serviceLocator.getUserService().removeByLogin(login);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    public UserDTO removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable User user = serviceLocator.getUserService().removeByLogin(id);
        @Nullable UserDTO userDTO = converterLocator.getUserConverter().toDTO(user);
        return userDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAllUsers(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable List<User> user = serviceLocator.getUserService().findAll();
        return user
                .stream()
                .map(converterLocator.getUserConverter()::toDTO)
                .collect(Collectors.toList());
    }

}
