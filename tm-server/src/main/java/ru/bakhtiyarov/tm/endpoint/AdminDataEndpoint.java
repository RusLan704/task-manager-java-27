package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IAdminDataEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.api.service.converter.IConverterLocator;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    @NotNull
    public AdminDataEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public AdminDataEndpoint() {
        super(null);
    }

    @Override
    @SneakyThrows
    public void loadBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBase64();
    }

    @Override
    @SneakyThrows
    public void clearBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearBase64();
    }

    @Override
    @SneakyThrows
    public void saveBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBase64();
    }

    @Override
    @SneakyThrows
    public void clearBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearBinary();
    }

    @Override
    @SneakyThrows
    public void loadBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBinary();
    }

    @Override
    @SneakyThrows
    public void saveBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBinary();
    }

    @Override
    @SneakyThrows
    public void clearJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearJson();
    }

    @Override
    @SneakyThrows
    public void loadJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadJson();
    }

    @Override
    @SneakyThrows
    public void saveJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveJson();
    }

    @Override
    @SneakyThrows
    public void clearXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearXml();
    }

    @Override
    @SneakyThrows
    public void loadXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadXml();
    }

    @Override
    @SneakyThrows
    public void saveXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveXml();
    }

    @Override
    @SneakyThrows
    public void clearYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearYaml();
    }

    @Override
    @SneakyThrows
    public void loadYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadYaml();
    }

    @Override
    @SneakyThrows
    public void saveYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveYaml();
    }

}
