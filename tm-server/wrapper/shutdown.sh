#!/bin/bash

echo  "SHUTDOWN TASK MANAGER SERVER...";
 if [ ! -f ./tm-server.pid ]; then  
	       echo "SERVER PID NOT FOUND"
         exit 1;
 fi
 echo "KILL PROCESS WITH PID "$(cat ./tm-server.pid);
 kill -9 $(cat ./tm-server.pid)
 rm ./tm-server.pid





