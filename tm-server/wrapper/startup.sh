#!/bin/bash

if [ -f ./tm-server.pid ]; then
  echo "Server already started!"
  exit 1;
fi

mkdir -p ../log
nohup java -jar ./tm-server.jar > ../log/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "STARTING TASK MANAGER SERVER WITH PID "$!