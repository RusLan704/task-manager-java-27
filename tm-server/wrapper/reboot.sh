#!/bin/bash

if [ ! -f ./tm-server.pid ]; then
  echo "SERVER PID NOT FOUND"
  exit 1;
fi

echo "KILL PROCESS WITH PID "$(cat ./tm-server.pid);
kill -9 $(cat ./tm-server.pid)
rm ./tm-server.pid

mkdir -p ../log
nohup java -jar ./tm-server.jar > ../log/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "STARTING TASK MANAGER SERVER WITH PID "$!