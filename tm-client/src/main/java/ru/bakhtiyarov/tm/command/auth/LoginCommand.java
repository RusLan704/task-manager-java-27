package ru.bakhtiyarov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.SessionEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login user in program";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();
        @NotNull final SessionDTO openedSession = sessionEndpoint.openSession(login, password);
        serviceLocator.getSessionService().setSession(openedSession);
        System.out.println("[OK]");
    }

}
