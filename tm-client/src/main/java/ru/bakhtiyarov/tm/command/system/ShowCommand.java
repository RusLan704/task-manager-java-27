package ru.bakhtiyarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.List;

public final class ShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (@NotNull AbstractCommand command : commands) System.out.println(command.name());
    }

}